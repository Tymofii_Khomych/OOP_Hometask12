﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // https://learn.microsoft.com/en-us/dotnet/standard/events/
            // Events

            /*
             * An event is a message sent by an object to signal the occurrence of an action. The action can be caused by user interaction, 
             * such as a button click, or it can result from some other program logic, such as changing a property's value. The object that 
             * raises the event is called the event sender. The event sender doesn't know which object or method will receive (handle) the 
             * events it raises. The event is typically a member of the event sender; for example, the Click event is a member of the Button 
             * class, and the PropertyChanged event is a member of the class that implements the INotifyPropertyChanged interface.
             * 
             * To define an event, you use the C# event or the Visual Basic Event keyword in the signature of your event class, 
             * and specify the type of delegate for the event. Delegates are described in the next section.
             * 
             * Typically, to raise an event, you add a method that is marked as protected and virtual (in C#) or Protected and Overridable 
             * (in Visual Basic). Name this method OnEventName; for example, OnDataReceived. The method should take one parameter that 
             * specifies an event data object, which is an object of type EventArgs or a derived type. You provide this method to enable 
             * derived classes to override the logic for raising the event. A derived class should always call the OnEventName method of 
             * the base class to ensure that registered delegates receive the event.
             * 
             * The following example shows how to declare an event named ThresholdReached. The event is associated with the
             * EventHandler delegate and raised in a method named OnThresholdReached.
             * 
             * class Counter
                {
                    public event EventHandler ThresholdReached;

                    protected virtual void OnThresholdReached(EventArgs e)
                    {
                        ThresholdReached?.Invoke(this, e);
                    }

                    // provide remaining implementation for the class
                }
            *
            * Event handlers
            * To respond to an event, you define an event handler method in the event receiver. This method must match the signature 
            * of the delegate for the event you're handling. In the event handler, you perform the actions that are required when the
            * event is raised, such as collecting user input after the user clicks a button. To receive notifications when the event occurs, 
            * your event handler method must subscribe to the event.
            * 
            * The following example shows an event handler method named c_ThresholdReached that matches the signature for the 
            * EventHandler delegate. The method subscribes to the ThresholdReached event.
            * 
            * class ProgramTwo
                {
                    static void Main()
                    {
                        var c = new Counter();
                        c.ThresholdReached += c_ThresholdReached;

                        // provide remaining implementation for the class
                    }

                    static void c_ThresholdReached(object sender, EventArgs e)
                    {
                        Console.WriteLine("The threshold was reached.");
                    }
                }

             */
        }
    }
}
