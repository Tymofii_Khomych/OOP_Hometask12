﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace task3
{
    class Model
    {
        public void Tick(object sender, EventArgs e, ref int i, Label label)
        {
            i++;
            label.Content = i.ToString();
        }
    }
}
