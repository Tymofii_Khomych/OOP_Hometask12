﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace task3
{
    class Presenter
    {
        Model model = null;
        MainWindow view = null;

        DispatcherTimer dt;
        private int time = 0;
        public Presenter(MainWindow mainWindow) { 
            model = new Model();
            view = mainWindow;
            view.startEvent += mainWindow_startEvent;
            view.stopEvent += mainWindow_stopEvent;
            view.resetEvent += mainWindow_resetEvent;
        }

        void mainWindow_startEvent(object sender, System.EventArgs e)
        {
            dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromSeconds(1);
            dt.Tick += (s, args) => model.Tick(sender, e, ref time, view.Timer);
            dt.Start();
        }

        void mainWindow_stopEvent(object sender, System.EventArgs e)
        {
            dt.Stop();
        }

        void mainWindow_resetEvent(object sender, System.EventArgs e)
        {
            time = 0;
            view.Timer.Content = time.ToString();
        }
    }
}
