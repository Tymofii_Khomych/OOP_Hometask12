﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class UserPresenter
    {
        private readonly IUserView view;
        private readonly UserModel model;

        public UserPresenter(IUserView view, UserModel model)
        {
            this.view = view;
            this.model = model;

            this.view.SaveButtonClicked += SaveButtonClickedHandler;
        }

        public void Start()
        {
            view.DisplayUserDetails(model.Name, model.Age);
        }

        private void SaveButtonClickedHandler()
        {
            model.Name = "John";
            model.Age = 30;

            view.DisplayUserDetails(model.Name, model.Age);
        }
    }
}
