﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    interface IUserView
    {
        void DisplayUserDetails(string name, int age);
        event Action SaveButtonClicked;
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            var userModel = new UserModel();
            var userView = new UserView();
            var userPresenter = new UserPresenter(userView, userModel);

            userPresenter.Start();
            userView.SimulateUserInput();
        }
    }
}
