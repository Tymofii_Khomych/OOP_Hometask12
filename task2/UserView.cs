﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class UserView : IUserView
    {
        public event Action SaveButtonClicked;

        public void DisplayUserDetails(string name, int age)
        {
            Console.WriteLine("User Details:");
            Console.WriteLine($"Name: {name}");
            Console.WriteLine($"Age: {age}");
        }

        public void SimulateUserInput()
        {
            SaveButtonClicked?.Invoke();
        }
    }
}
