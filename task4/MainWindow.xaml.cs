﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace task4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            new Presenter(this);
        }

        public event EventHandler sum_event = null;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            sum_event?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler sub_event = null;
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            sub_event?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler mul_event = null;
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            mul_event?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler div_event = null;
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            div_event?.Invoke(this, EventArgs.Empty);
        }
    }
}
