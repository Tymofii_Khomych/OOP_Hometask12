﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace task4
{
    internal class Presenter
    {
        Model model = null;
        MainWindow view = null;

        public Presenter(MainWindow mainWindow)
        {
            model = new Model();
            view = mainWindow;
            view.sum_event += mainWindow_sum_event;
            view.sub_event += mainWindow_sub_event;
            view.mul_event += mainWindow_mul_event;
            view.div_event += mainWindow_div_event;
        }

        void mainWindow_sum_event(object sender, System.EventArgs e)
        {
            view.ResultLabel.Content = $"{view.textBox1.Text} + {view.textBox2.Text} = " + Convert.ToString
                (model.Sum(Convert.ToDouble(view.textBox1.Text), Convert.ToDouble(view.textBox2.Text)));
        }

        void mainWindow_sub_event(object sender, System.EventArgs e)
        {
            view.ResultLabel.Content = $"{view.textBox1.Text} - {view.textBox2.Text} = " + Convert.ToString
               (model.Sub(Convert.ToDouble(view.textBox1.Text), Convert.ToDouble(view.textBox2.Text)));
        }

        void mainWindow_mul_event(object sender, System.EventArgs e)
        {
            view.ResultLabel.Content = $"{view.textBox1.Text} * {view.textBox2.Text} = " + Convert.ToString
               (model.Mul(Convert.ToDouble(view.textBox1.Text), Convert.ToDouble(view.textBox2.Text)));
        }

        void mainWindow_div_event(object sender, System.EventArgs e)
        {
            view.ResultLabel.Content = $"{view.textBox1.Text} / {view.textBox2.Text} = " + Convert.ToString
               (model.Div(Convert.ToDouble(view.textBox1.Text), Convert.ToDouble(view.textBox2.Text)));
        }
    }
}
